package api;

import java.io.FileNotFoundException;

import model.data_structures.IList;

import model.vo.VORoute;
import model.vo.VOService;
import model.vo.VOStop;
import model.vo.VOTrip;


/**
 * Basic API for testing the functionality of the STS manager
 */
public interface ISTSManager {
	/**
	 * Carga toda la información estática necesaria para la operación del sistema. 
	 * (Archivo de rutas, viajes, paradas, etc.)
	 * @throws FileNotFoundException 
	 */
	void ITScargarGTFS( ) throws FileNotFoundException;
	/**
	 * Carga la información en tiempo real de los buses en para fecha determinada.
	 */
	void ITScargarTR(String fecha);

	/**
	 * Retorna una lista de rutas que son prestadas por una empresa determinada, enuna fecha determinada.
	 */

	IList<VORoute> ITSrutasPorEmpresa(String nombreEmpresa, String fecha);

	/**
	 * Retorna una lista de viajes retrasados para una ruta en una fecha determinada.
	 */
	IList<VOTrip> ITSviajesRetrasadosRuta(String idRuta, String fecha);

	/**
	 * Retorna una lista de Paradas en las que hubo un retraso en una fecha dada
	 */
	IList<VOStop> ITSparadasRetrasadasFecha( String fecha);

	/**
	 * Retorna una lista de paradas, las cuales conforman todos los posibles transbordos que se derivan a partir de la primera parada de dicha ruta.
	 */
	IList<IList<VOStop>> ITStransbordosRuta(String idRuta);
	
	/**
	 * Retorna una lista en la que en cada posición se tiene una lista con la información de una parada específica. 
	 * A su vez en cada posición de esta lista, se tiene una lista con los viajes de la parada que se está analizando 
	 */
	IList <IList<IList<VOTrip>>> ITSrutasPlanUtilizacion (String fecha, String horaInicio, String horaFin);
	
	/**
	 * Retorna una lista de rutas que son prestadas por una empresa determinada, en una fecha determinada. 
	 * La lista está ordenada por cantidad total de paradas.
	 */
	IList<VORoute>ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha);
	
	/**
	 * Retornar una lista con todos los viajes en los que después de un retardo, todas las paradas siguientes 
	 * tuvieron retardo, para una ruta específica en una fecha específica. Se debe retornar una lista ordenada por el id 
	 * del viaje, y la localización de las paradas, de mayor a menor en tiempo de retardo.
	 */
	IList<VOTrip> ITSviajesRetrasoTotalRuta(String idRuta, String fecha);
	
	/**
	 * Retorna una lista ordenada con rangos de hora en los que mas retardoshubo.
	 * @throws FileNotFoundException 
	 */
	IList<VOTrip> ITSretardoHoraRuta (String idRuta, String Fecha ) throws FileNotFoundException;
	
	
	/**
	 * Retorna la ruta que tiene menos retardo promedio en distancia de sus viajes en una fecha dada. 
	 * El retardo promedio en distancia de un viaje corresponde al total de los tiempos de retardo de sus paradas dividido por la distancia recorrida.
	 */
	IList ITSrutaMenorRetardo (String idRuta, String fecha);
	
	/**
	 * Inicializa las estructuras de datos necesarias
	 */
	Void ITSInit();
	
	/**
	 * Retorna una lista con los servicios que mas distancia recorren en una fecha. La lista está ordenada por distancia.
	 */
	IList<VOService> ITSserviciosMayorDistancia (String fecha);
	
	/**
	 * Retorna una lista con todos los retardos de un viaje en una fecha dada.
	 */
	IList<RetardoVO> ITSretardosViaje (String fecha, String idViaje);
	
	/**
	 * Retorna una lista con todas las paradas del sistema que son compartidas por mas de una ruta en una fecha determinada.
	 */
	IList <VOStop>ITSparadasCompartidas (String fecha);
	/**
	 * Retorna una lista con los viajes para ir de la parada de inicio a la parada final en una fecha y franja horaria determinada.
	 */
	IList<VOTrip> ITSbuscarViajesParadas(int idOrigen, int idDestino, String fecha, String horaInicio, String horaFin)
			throws FileNotFoundException;
	
}
