package model.data_structures;

public class Stack<T> implements IStack<T>{

	

	private int size;
	private NodoListaDoblementeEncadenada<T> node;
	
	public Stack() {
		size = 0;
		node = null;
	}
	
	public int getSize() 
	{
		return size;
	}
	
	public boolean isEmpty() 
	{
		return size == 0;
	}
	
	public void push(T item) 
	{
		NodoListaDoblementeEncadenada<T> newNode = new NodoListaDoblementeEncadenada<T>(item);
		if (size == 0) 
		{
			node = newNode;
			size++;
		}
		else 
		{
			newNode.cambiarAnterior(node);
			node.cambiarSiguiente(newNode);
			node = newNode;
			size++;
		}
		
	}

	@Override
	public T pop() 
	{
		T element = null;
		if (isEmpty()) 
		{
			
		}
		else if (size == 1) 
		{
			element = node.darElemento();
			size--;
			node = null;
		}
		else 
		{
			element = node.darElemento();
			NodoListaDoblementeEncadenada<T> temp = node.darAnterior();
			temp.cambiarSiguiente(null);
			size--;
			node = temp;
		}
		
		return element;
	}

}
