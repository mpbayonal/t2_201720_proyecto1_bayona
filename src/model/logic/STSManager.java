package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ISTSManager;

import model.vo.VOAgency;
import model.vo.VOBusUpdate;
import model.vo.VOEspecialService;
import model.vo.VOInfo;
import model.vo.VORoute;
import model.vo.VORouteEstimate;
import model.vo.VOService;
import model.vo.VOShape;
import model.vo.VOStop;
import model.vo.VOStopTime;
import model.vo.VOTransfer;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Queue;
import model.data_structures.RingList;

public class STSManager implements ISTSManager {

	private DoubleLinkedList<VORoute> routes;
	private DoubleLinkedList<VOStopTime> stopTimes;
	private DoubleLinkedList<VOAgency> agencies;
	private DoubleLinkedList<VOEspecialService> especialServices;
	private DoubleLinkedList<VOService> services;
	private DoubleLinkedList<VOTransfer> transfers;
	private DoubleLinkedList<VOShape> shapes;
	private DoubleLinkedList<VOInfo> feed_info;

	private RingList<VOTrip> trips;
	private RingList<VOStop> stops;
	
	private Queue<VOBusUpdate> updates;
	private DoubleLinkedList<VORouteEstimate> estimate;
	private PersistenceManager persistencia;

	@Override
	public void ITScargarGTFS() throws FileNotFoundException 
	{
		persistencia = new PersistenceManager();
		
		routes = persistencia.loadRoutes("./data/routes.txt");
		stopTimes = persistencia.loadStopTimes("./data/stop_times.txt");
		agencies = persistencia.loadAgencies("./data/agency.txt");
		especialServices = persistencia.loadEspecialServices("./data/calendar_dates.txt");
		feed_info = persistencia.loadInfo("./data/calendar_dates.txt");
		services = persistencia.loadServices("./data/calendar.txt");
		shapes = persistencia.loadShapes("./data/shapes.txt");
		stops = persistencia.loadStops("./data/stops.txt");
		transfers = persistencia.loadTransfers("./data/transfers.txt");
		trips = persistencia.loadTrips("./data/trips.txt");
		updates = persistencia.readBusUpdate("./data/BUSES_SERVICE");
		estimate = persistencia.readStopsEstimateService("./data/STOPS_ESTIM_SERVICES");
				
	}

	@Override
	public void ITScargarTR(String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VORoute> ITSrutasPorEmpresa(String nombreEmpresa, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOTrip> ITSviajesRetrasadosRuta(String idRuta, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOStop> ITSparadasRetrasadasFecha(String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<IList<VOStop>> ITStransbordosRuta(String idRuta) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<IList<IList<VOTrip>>> ITSrutasPlanUtilizacion(String fecha, String horaInicio, String horaFin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VORoute> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOTrip> ITSviajesRetrasoTotalRuta(String idRuta, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
public IList<VOTrip> ITSretardoHoraRuta(String idRuta, String fecha) throws FileNotFoundException {
		
		
		ITScargarTR(fecha);
		boolean e= false;
		DoubleLinkedList<VOTrip> rTrips= new DoubleLinkedList<VOTrip>();
		
		
		 Iterator<VORoute> iRuta = routes.iterator();
	        
	        while (iRuta.hasNext() && !e)
	        {
	            VORoute ruta = iRuta.next();
	            if(ruta.getRouteShortName().equals(idRuta))
	            {
	            		e=true;
	            		Iterator<VOTrip>  iTrip= trips.iterator();
	            		while(iTrip.hasNext()) {
	            			VOTrip trip= iTrip.next();
	            			if(ruta.getRouteShortName().equals(trip.getRouteId())) {
	            				//TODO saber si estuvo tarde! y el total del tiempo en retardos.
	            				
	            			}
	            		}
	            		
	            }

	        }
	        return rTrips;
	    }

		
	

	@Override
	public IList<VOTrip> ITSbuscarViajesParadas(int idOrigen, int idDestino, String fecha, String horaInicio,
			String horaFin) throws FileNotFoundException {
		
		ITScargarTR(fecha);
		
		DoubleLinkedList<VOTrip> rTrips= new DoubleLinkedList<VOTrip>();
		
		 Iterator<VOTrip> iTrip = trips.iterator();
	        
	        while (iTrip.hasNext())
	        {
	            VOTrip trip = iTrip.next();
	            Iterator<VOStopTime> iSTime= stopTimes.iterator();
	            
	            while(iSTime.hasNext()) {
	            	VOStopTime sTime= iSTime.next();
	            	if(sTime.getTripId()==trip.id()) {
	            		if(sTime.getStopId()==idOrigen) {
	            			if(sTime.getArrivalTime().compareTo(horaInicio)>0 && sTime.getArrivalTime().compareTo(horaFin)<0) {
	            				
	            			}
	            		}
	            		
	            	}
	            	
	            }
	        }
	        return rTrips;
	    }

	@Override
	public IList ITSrutaMenorRetardo(String idRuta, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Void ITSInit() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOService> ITSserviciosMayorDistancia(String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<RetardoVO> ITSretardosViaje(String fecha, String idViaje) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOStop> ITSparadasCompartidas(String fecha) {
		// TODO Auto-generated method stub
		return null;
	}


	

}
