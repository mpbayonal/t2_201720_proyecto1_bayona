package model.vo;

/**
 * Representation of a route object
 */
public class VORoute implements Comparable<VORoute>{

    // -----------------------------------------------------------------
    // Attributes
    // -----------------------------------------------------------------

    private int routeId;
    private String agencyId;
    private String routeShortName;
    private String routeLongName;
    private String routeDesc;
    private int routeType;
    private String routeUrl;
    private Integer routeColor;
    private String routeTextColor;

    // -----------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------

    /**
     * @return id - Route's id number
     */
    public VORoute(int pRouteId , String pAgencyId, String pRouteShortName, String pRouteLongName, String pRouteDesc, int pRouteType, String pRouteUrl, Integer pRouteColor, String pRouteTextColor)
    {
        routeId = pRouteId;
        agencyId = pAgencyId;
        routeShortName = pRouteShortName;
        routeLongName = pRouteLongName;
        routeDesc = pRouteDesc;
        routeType = pRouteType;
        routeUrl = pRouteUrl;
        routeColor = pRouteColor;
        routeTextColor = pRouteTextColor;
    }

    // -----------------------------------------------------------------
    // Methods - Getters
    // -----------------------------------------------------------------

    public int id() {

        return routeId;
    }

    /**
     * @return name - route name
     */
    public String getName() {

        return routeShortName;
    }

    public int getRouteId()
    {
        return routeId;
    }

    public String getAgencyId()
    {
        return agencyId;
    }

    public String getRouteShortName()
    {
        return routeShortName;
    }

    public String getRouteLongName()
    {
        return routeLongName;
    }

    public String getRouteDesc()
    {
        return routeDesc;
    }

    public int getRouteType()
    {
        return routeType;
    }

    public String getRouteUrl()
    {
        return routeUrl;
    }

    public Integer getRouteColor()
    {
        return routeColor;
    }

    public String getRouteTextColor()
    {
        return routeTextColor;
    }

    // -----------------------------------------------------------------
    // Methods - Setters
    // -----------------------------------------------------------------

    public void setRouteId(int newValue)
    {
        routeId = newValue;
    }

    public void setAgencyId(String newValue)
    {
        agencyId = newValue;
    }

    public void setRouteShortName(String newValue)
    {
        routeShortName = newValue;
    }

    public void setRouteLongName(String newValue)
    {
        routeLongName = newValue;
    }

    public void setRouteDesc(String newValue)
    {
        routeDesc = newValue;
    }

    public void setRouteType(int newValue)
    {
        routeType = newValue;
    }

    public void setRouteUrl(String newValue)
    {
        routeUrl = newValue;
    }

    public void setRouteColor(Integer newValue)
    {
        routeColor = newValue;
    }

    public void setRouteTextColor(String newValue)
    {
        routeTextColor = newValue;
    }

    // -----------------------------------------------------------------
    // Methods - Logic
    // -----------------------------------------------------------------

    public int compareTo(VORoute n) {
        int r;

        if(n.id() == routeId)
        {
            r=0;
        }
        else if(n.id() < routeId)
        {
            r=1;
        }
        else
        {
            r=-1;
        }

        return r;
    }
}
