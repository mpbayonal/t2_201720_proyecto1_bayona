package model.vo;

import com.google.gson.annotations.SerializedName;

public class VOBusUpdate {
	
	
	public VOBusUpdate(String vehicleNo, int tripId, String routeNo, String direction, String destination,
			String pattern, double latitude, double longitude, String recordedTime, UrlRoute routeMap) {
		super();
		VehicleNo = vehicleNo;
		TripId = tripId;
		RouteNo = routeNo;
		Direction = direction;
		Destination = destination;
		Pattern = pattern;
		Latitude = latitude;
		Longitude = longitude;
		RecordedTime = recordedTime;
		RouteMap = routeMap;
	}
	@SerializedName("VehicleNo") private String VehicleNo;
	@SerializedName("TripId") private int TripId;
	@SerializedName("RouteNo") private String RouteNo;
	@SerializedName("Direction") private String Direction;
	@SerializedName("Destination") private String Destination;
	@SerializedName("Pattern") private String Pattern;
	@SerializedName("Latitude") private double Latitude;
	@SerializedName("Longitude") private double Longitude;
	@SerializedName("RecordedTime") private String RecordedTime;
	@SerializedName("RouteMap") private UrlRoute RouteMap;
	
}
