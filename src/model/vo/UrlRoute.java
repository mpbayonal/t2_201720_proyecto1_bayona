package model.vo;

import com.google.gson.annotations.SerializedName;

public class UrlRoute 
{
	@SerializedName("Href") private String Href;

	public UrlRoute(String href) {

		Href = href;
	}

	public String getHref() {
		return Href;
	}

	public void setHref(String href) {
		Href = href;
	}

}
