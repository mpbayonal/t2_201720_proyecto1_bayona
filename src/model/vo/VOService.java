package model.vo;

public class VOService implements Comparable<VOService> 
{
	public VOService(int id, int monday, int tuesday, int wednesday, int thursday, int friday, int saturday, int sunday,
			int start_date, int end_date) {
		super();
		this.id = id;
		this.monday = monday;
		this.tuesday = tuesday;
		this.wednesday = wednesday;
		this.thursday = thursday;
		this.friday = friday;
		this.saturday = saturday;
		this.sunday = sunday;
		this.start_date = start_date;
		this.end_date = end_date;
	}

	private int id;
	private int monday;
	private int tuesday;
	private int wednesday;
	private int thursday;
	private int friday;
	private int saturday;
	private int sunday;
	private int start_date;
	private int end_date;

	@Override
	public int compareTo(VOService o) {
		if(id < o.getId()) {
			return -1;}
		else if(id > o.getId()) {
			return 1;}
		else {
		return 0;}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMonday() {
		return monday;
	}

	public void setMonday(int monday) {
		this.monday = monday;
	}

	public int getTuesday() {
		return tuesday;
	}

	public void setTuesday(int tuesday) {
		this.tuesday = tuesday;
	}

	public int getWednesday() {
		return wednesday;
	}

	public void setWednesday(int wednesday) {
		this.wednesday = wednesday;
	}

	public int getThursday() {
		return thursday;
	}

	public void setThursday(int thursday) {
		this.thursday = thursday;
	}

	public int getFriday() {
		return friday;
	}

	public void setFriday(int friday) {
		this.friday = friday;
	}

	public int getSaturday() {
		return saturday;
	}

	public void setSaturday(int saturday) {
		this.saturday = saturday;
	}

	public int getSunday() {
		return sunday;
	}

	public void setSunday(int sunday) {
		this.sunday = sunday;
	}

	public int getStart_date() {
		return start_date;
	}

	public void setStart_date(int start_date) {
		this.start_date = start_date;
	}

	public int getEnd_date() {
		return end_date;
	}

	public void setEnd_date(int end_date) {
		this.end_date = end_date;
	}

}
